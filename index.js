var wol = require("wol");
var PushBullet = require("pushbullet");

/* --------------------------------------------------*/

// Your PushBullet Access Token goes here:
// (https://www.pushbullet.com/#settings)
var pusher = new PushBullet("PUSHBULLET_API_KEY");

// Your PC's MAC Address goes here:
let macAddress = "99:99:99:99:99:99";

// And your PushBullet message goes here:
let pushMessage = "WOL_PC";

/* --------------------------------------------------*/

pusher.me((err, response) => {
  err
    ? console.log(`Couldn't sign in. Error: ${err}`)
    : console.log(`Logged in as  + ${response.email}`);
});

let stream = pusher.stream();

stream.on("tickle", () => {
  pusher.history({ limit: 1 }, (err, response) => {
    err ? console.log(`An error occured retrieving latest push: ${err}`) : null;

    if (response.pushes[0].body === pushMessage) {
      console.log(`I got a bite! Waking ${macAddress}!`);
      wol.wake(macAddress, res => {
        res
          ? console.log(`Wakey Wakey, ${macAddress}!`)
          : console.log(
              `Something went wrong... ${macAddress} probably didn't wake up`
            );
      });
    }
  });
});

stream.connect();
