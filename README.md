# PushBullet Wake On LAN

This tiny script lets you use PushBullet to turn on your PC

## Why make this?

A friend was juggling several apps on his phone, as well as forwarding ports on his router, to allow himself to turn on his PC via Google Assistant. It kinda looked like this:

`Google Assistant => IFTTT => PushBullet => Android Notifications => Automate => Magic Packet => Home Network`

I took one look at this and said "Why not have something inside your network listen to PushBullet and send the magic packet?".

Now this runs on their always-on RPi, waiting for the PushBullet message to come through, without needing to do dangerous port forwarding and other messy things!

## Installation

Clone the repo, and install the dependencies:

```bash
npm install
```

## Usage

You'll need your PushBullet Access Token. You can get that from your [PushBullet Settings](https://www.pushbullet.com/#settings)

Modify the PushBullet API Key, MAC address to match your target PC's MAC address, and the pushMessage to match your PushBullet's body message.

```js
// Your PushBullet Access Token goes here:
// (https://www.pushbullet.com/#settings)
var pusher = new PushBullet("PUSHBULLET_API_KEY");

// Your PC's MAC Address goes here:
let macAddress = "99:99:99:99:99:99";

// And your PushBullet message goes here:
let pushMessage = "WOL_PC";
```

You may need to consult your operating system's documentation, as well as your NIC/Motherboard manual, to enable Wake On LAN first.

Then, simply run `node index.js`

## Contributing

Pull requests are always delight to receive!

## License

[MIT](https://choosealicense.com/licenses/mit/)
